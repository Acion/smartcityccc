BEGIN;

CREATE TABLE event (
    id SERIAL,
    src TEXT DEFAULT '' NOT NULL,
    srcurl TEXT DEFAULT '' NOT NULL,
    name TEXT DEFAULT '' NOT NULL,
    summary TEXT DEFAULT '' NOT NULL,
    description TEXT DEFAULT '' NOT NULL,
    location TEXT DEFAULT '' NOT NULL,
    dayfrom DATE DEFAULT '-infinity'::date NOT NULL,
    dayto DATE DEFAULT 'infinity'::date NOT NULL,
    every TEXT DEFAULT '' NOT NULL,
    eventtime TEXT DEFAULT '' NOT NULL,
    categories TEXT DEFAULT '' NOT NULL,
    website TEXT DEFAULT '' NOT NULL,
    facebook TEXT DEFAULT '' NOT NULL,
    pricing TEXT DEFAULT '' NOT NULL,
    imgurl TEXT DEFAULT '' NOT NULL,
    thumb TEXT DEFAULT '' NOT NULL,
    featured BOOLEAN DEFAULT FALSE NOT NULL
);



--CREATE TABLE status (
--    id SERIAL,
--    feeder TEXT DEFAULT '' NOT NULL,
--    updated TIMESTAMP DEFAULT now() NOT NULL
--);

CREATE TABLE rating (
    id SERIAL,
    score INTEGER
);



COMMIT;

