from flask import Flask,render_template,request,redirect


import store
import os
import sys

app = application = Flask(__name__)

app.config.update(
    DEBUG=True,
)

if os.getenv('SMARTDB','') == '':
    print("$SMARTDB is unset, so running without database (ie dummy data only!)", file=sys.stderr)

@app.route("/")
def index(name=None):

    if os.getenv('SMARTDB','') != '':

        # TODO: fix store to use 'with' syntax here!
        db = store.connect()
        dat = db.fetch_latest()
        db.close()
    else:
        # running without db - stub data:
        dat = {'events': {} }

    # show only featured events
    events = dat['events']
    events = [ev for ev in events if ev['featured']]
    dat['events'] = events

    return render_template("index.html",dat=dat)


@app.route("/stuff")
def stuff(name=None):
    return render_template("stuff.html")

@app.route("/buses")
def buspage(name=None):
    return render_template("buses.html")

@app.route("/waterquality")
def waterqualitypage(name=None):
    return render_template("waterquality.html")

@app.route("/air")
def airpage(name=None):
    return render_template("air.html")


@app.route('/rate', methods = ['POST',])
def user():
    # TODO: use a cookie as a first pass to limit vote rate
    score = request.form['score']

    # TODO: fix store to use 'with' syntax here!
    assert( score in ("1","0","-1") )
    db = store.connect()
    db.submit_rating(score)
    db.close()
    return redirect('/')


if __name__ == "__main__":
    application.run()

