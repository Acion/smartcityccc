import psycopg2
import os

from .event import fetch_events
from .rating import fetch_rating

def connect():
    return Connection()

class Connection:
    def __init__(self):
        self.connstr = os.getenv('SMARTDB','')
        self.conn = psycopg2.connect(self.connstr)

    def __enter__(self):
        pass

    def __exit__(self, type, value, tb):
        self.close()

    def close(self):
        self.conn.close()

    def fetch_latest(self):
        with self.conn.cursor() as cur:
            events = fetch_events(cur)
            rating = fetch_rating(cur)
            return {
                    "events": events,
                    "rating": rating,
                    }

    def submit_rating(self, score):
        """ very very noddy rating system! """
        with self.conn.cursor() as cur:
            # TODO: add timestamps and double-vote protection and whatnot
            cur.execute("""INSERT INTO rating (score) VALUES (%s)""", (score,))
            self.conn.commit()


