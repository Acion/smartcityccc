from datetime import date,timedelta


def fetch_events(curs):
    """ grab all events which intersect viewing range """

    dayfrom = date.today()
    dayto = dayfrom + timedelta(days=7)

#    q = """
#SELECT name,summary,description,location,dayfrom,dayto,every,eventtime,categories,website,facebook,pricing,imgurl,thumb,featured
#    FROM event
#    WHERE dayfrom<=%s AND dayto>=%s
#"""
#    curs.execute(q, (dayto, dayfrom))

    q = """
SELECT srcurl,name,summary,description,location,dayfrom,dayto,every,eventtime,categories,website,facebook,pricing,imgurl,thumb,featured
    FROM event
    WHERE featured=TRUE
"""
    curs.execute(q)

    events = []
    for row in curs:
        ev = {
            "srcurl": row[0],
            "name": row[1],
            "summary": row[2],
            "description": row[3],
            "location": row[4],
            "dayfrom": row[5],
            "dayto": row[6],
            "every": row[7],
            "time": row[8],
            "categories": row[9],   # TODO: split into list
            "website": row[10],
            "facebook": row[11],
            "pricing": row[12],
            "imgurl": row[13],
            "thumb": row[14],
            "featured": row[15],
            }
        events.append(ev)

    return present_events(events, dayfrom, dayto)


dow_lookup = { 'monday':0,
        'tuesday':1,
        'wednesday':2,
        'thursday':3,
        'friday':4,
        'saturday':5,
        'sunday':6 }


def present_events(events, dayfrom, dayto):
    """ assign a display day to each event, based on viewing range"""

    out = []
    for ev in events:
        if ev['dayfrom'] == ev['dayto']:
            # it's a single day event
            ev['day'] = ev['dayfrom']
            out.append(ev)
            continue

        # event is a date range
        if ev['every'] == '':
            # place on day it starts, but clip to viewing range
            ev['day'] = max(ev['dayfrom'], dayfrom)
            out.append(ev)
            continue
        else:
            # a weekly-repeating event:
            # generate an instance of the event for every matching day
            # within our viewing range
            dow = dow_lookup[ev['every']]
            day = dayfrom
            while day < dayto:
                if day.weekday() == dow:
                    phantom = ev.copy()
                    phantom['day'] = day
                    out.append(phantom)
                day = day + timedelta(days=1)

    # return sorted by day

    return sorted(out, key=lambda x: x['day'])




