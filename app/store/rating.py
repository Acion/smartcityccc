




def fetch_rating(cur):

    scores = [0,0,0]

    cur.execute("""select score,count(score) from rating group by score""")
    for row in cur:
        score,cnt= row[0],row[1]
        if score==1:
            scores[0]= cnt
        if score==0:
            scores[1]= cnt
        if score==-1:
            scores[2]= cnt

    tot = sum(scores)
    if tot == 0:
        out = [0,0,0]
    else:
        out = [ (s*100)/tot for s in scores ]
    return {
        "1": out[0],
        "0": out[1],
        "-1": out[2],
        "votes": tot
        }

