#!/usr/bin/env python3

# hacky tool to quickly sort icon layout and images

import sys
import os
from PIL import Image


# All dimensions are based off layout design image, scaled 1000px wide.
# then just measure icon position and size with the selection tool.

icon_area = (1000,350)
src_dir = "../srcgfx/2016-11-22/PNGs"
dest_dir = "/tmp/icons"


grp0 = "default"    # will be hidden in mobile view
grp_sky = "sky"
grp_grass = "grass"
grp_road = "road"
grp_water = "water"

all_grps = [grp_sky,grp_grass,grp_road,grp_water,grp0]

# (ident,href,grp,filename,x,y,w,h)
srcs = [
    ("plane","/stuff",grp_sky,"Christchurch-Smart-City_0000_plane.png",119,41,88,37),
    ("sun","/stuff",grp_sky,"Christchurch-Smart-City_0001_sun.png",316,47,53,53),
    ("clouds","/air",grp_sky,"Christchurch-Smart-City_0002_clouds.png",578,53,182,64),
    ("bus","/buses",grp_road,"Christchurch-Smart-City_0003_bus.png",836,272,101,52),
    ("car","/stuff",grp_road,"Christchurch-Smart-City_0004_car.png",37,252,87,47),
    ("canoe","/waterquality",grp_water,"Christchurch-Smart-City_0005_canoe.png",192,304,84,43),
    ("scooter","/stuff",grp0,"Christchurch-Smart-City_0006_girl-on-scooter.png",547,234,30,42),
    ("bike","/stuff",grp_road,"Christchurch-Smart-City_0007_biker-on-bike.png",682,268,65,59),
    ("tourists","/stuff",grp_grass, "Christchurch-Smart-City_0008_tourists.png",777,223,48,57),
    ("boatsheds","/stuff",grp0, "Christchurch-Smart-City_0009_antigua-boatsheds.png", 144,217,133,51),
    ("antarctica","/stuff",grp0, "Christchurch-Smart-City_0010_antarctica.png", 103,178,124,41),
    ("shops","/stuff",grp_grass, "Christchurch-Smart-City_0011_shops.png", 227,161,70,42),
    ("remembrance","/stuff",grp0, "Christchurch-Smart-City_0012_bridge-of-remembrance.png", 523,182,78,57),
    ("ccc","/stuff",grp_grass, "Christchurch-Smart-City_0013_ccc.png", 423,147,119,67),
    ("art","/stuff",grp_grass, "Christchurch-Smart-City_0014_art-gallery.png", 297,141,148,70),
    ("chalice","/stuff",grp_grass, "Christchurch-Smart-City_0015_chalice-sculpture.png", 594,154,53,98),
    ("cathedral","/stuff",grp_grass, "Christchurch-Smart-City_0016_cardboard-cathedral.png", 643,165,86,73),
    ("garden","/stuff",grp_grass, "Christchurch-Smart-City_0017_garden.png", 305,215,67,42),
    ("clocktower","/stuff",grp0,"Christchurch-Smart-City_0018_clock-tower.png", 733,172,19,55),
    ("pier","/stuff",grp_water,"Christchurch-Smart-City_0019_pier.png",751,188,224,34),
    ("trafficlight","/stuff",grp0,"Christchurch-Smart-City_0020_traffic-light.png", 757,233,13,64),
    ("tram","/stuff",grp0,"Christchurch-Smart-City_0021_tram.png",378,225,80,37),
    ("tramtrack","/stuff",grp0,"Christchurch-Smart-City_0022_tram-track.png",273,257,482,7),
    ("houses1","/stuff",grp_grass,"Christchurch-Smart-City_0023_houses-1.png",17,138,50,37),
    ("houses2","/stuff",grp_grass,"Christchurch-Smart-City_0024_houses-2.png",75,142,108,43),
    ("crane","/stuff",grp_grass, "Christchurch-Smart-City_0025_crane-yellow.png",439,56,110,117),
    # TODO: merge cranes!
    #Christchurch-Smart-City_0026_crane-red.png
#Christchurch-Smart-City_0027_trees-1.png
#Christchurch-Smart-City_0028_trees-2.png
    ("trees3","/stuff",grp_grass,"Christchurch-Smart-City_0029_trees-3.png",520,140,203,71),
#Christchurch-Smart-City_0030_trees-4-.png
#Christchurch-Smart-City_0031_road.png
#Christchurch-Smart-City_0032_land.png
#Christchurch-Smart-City_0033_mountain.png
#Christchurch-Smart-City_0034_ocean-river.png
#Christchurch-Smart-City_0035_sky.png
    ]

increased = """<img alt="increased" src="/static/increased.png" /> """
decreased = """<img alt="decreased" src="/static/decreased.png" /> """

captions = {
        # ident: (class, content)
        'sun': ("cap cap-t", """<span class="cap-maj">2x10<sup>30</sup></span> <small class="cap-min">kg hydrogen remaining</small>"""),
        'clouds': ("cap cap-t", """<span class="cap-maj">""" + increased + """<span class="sev sev-red">High</span></span> <small class="cap-min">Air pollution today</small>"""),

        'car': ("cap", """<span class="cap-maj"><span class="sev sev-green">Light</span></span> <small class="cap-min">Traffic today</small>"""),
        'crane': ("cap cap-t", """<span class="cap-maj">""" + increased + """2,790</span> <small class="cap-min">Constructions underway</small>"""),
        'trees3': ("cap cap-t", """<span class="cap-maj"><span class="sev sev-orange">Medium</span></span> <small class="cap-min">Fire danger today</small>"""),
        'bus': ("cap", """<span class="cap-maj">""" + increased + """4,612</span> <small class="cap-min">Took the bus today</small>""" ),
        'plane': ("cap", """<span class="cap-maj">""" + decreased + """18</span> <small class="cap-min">Planes in Christchurch Airport</small>"""),
        'pier': ("cap cap-t", """<span class="cap-maj"><span class="sev sev-orange">OK</span></span> <small class="cap-min">surf: 7, choppy</small>""" ),
        'shops': ("cap cap-t", """<span class="cap-maj">38,950</span> <small class="cap-min">Businesses operating in Christchurch</small>""" ),
        'tourists': ("cap", """<span class="cap-maj">26,585</span> <small class="cap-min">Visitors currently in Christchurch</small>""" ),
        'art': ("cap cap-t", """<span class="cap-maj">""" + decreased + """3,193</span> <small class="cap-min">Visitors to Art Gallery today</small>""" ),
        'garden': ("cap", """<span class="cap-maj">Roses</span> <small class="cap-min">currently in bloom at the Botanical Gardens</small>""" ),
        'canoe': ("cap cap-t", """<span class="cap-maj"><span class="sev sev-green">Good</span></span> <small class="cap-min">water quality</small>""" ),
    }



# sort by y-pos (should give us mostly-correct ordering...)
srcs.sort(key=lambda foo: foo[5])

def generate_css():

    (areaw,areah) = icon_area
    for (ident,href,grp,filename,x,y,w,h) in srcs:
        x = (x*100)/areaw
        y = (y*100)/areah
        w = (w*100)/areaw

        print( """    .icon-%s {left: %.2g%%; top: %.2g%%; width: %.2g%%;}""" % (ident,x,y,w))


def generate_html():
    for curgrp in  all_grps:
        print("""<ul class="icon-grp icon-grp-%s">""" % (curgrp,))
        for (ident,href,grp,filename,x,y,w,h) in srcs:
            if grp != curgrp:
                continue
            cap = ""
            if ident in captions:
                cls,txt = captions[ident]
                cap = """<div class="%s">%s</div>""" %(cls,txt)
            print( """  <li class="icon icon-%s"><a href="%s"><img alt="%s" class="icon-img" src="/static/icon/%s.png" />%s</a></li>""" % (ident,href,ident,ident,cap) )
        print("""</ul> <!-- end .icon-grp-%s -->\n""" % (curgrp,) )



def cook_pngs():
    # scale the images down into a sensible size, based upon the size
    # they appear on the page.
    scale = 2
    for (ident,href,grp,filename,x,y,w,h) in srcs:
        infile = os.path.join(src_dir,filename)
        outfile = os.path.join(dest_dir, ident + ".png")
        im = Image.open(infile)
        im2 = im.resize( (int(w*scale),int(h*scale)),resample=Image.LANCZOS)
        im2.save(outfile)
        print( outfile)

def usage():
    print("usage: %s [png|html|css]" % (sys.argv[0],), file=sys.stderr)

def main():
    if len(sys.argv) < 2:
        usage()
        sys.exit(1)

    if sys.argv[1] == "png":
        cook_pngs()
    elif sys.argv[1] == "css":
        generate_css()
    if sys.argv[1] == "html":
        generate_html()
    else:
        usage()
        sys.exit(1)



main()


